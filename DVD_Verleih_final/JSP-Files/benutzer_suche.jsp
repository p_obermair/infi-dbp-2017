<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="DB_Manager_package.DB_Manager"%>
    <%@page import="DB_Manager_package.DB_Manager"%>
    <%@page import="java.sql. *"%>
    <%@page import="org.sqlite. *"%>
    <%Class.forName("org.sqlite.JDBC"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DVD-Verleih</title>
</head>
<body>
<h1>Ergebnis der Suche:</h1>
<table>
<thead>
	<tr>
		<th>Benutzer-ID</th>
		<th>Vorname</th>
		<th>Nachname</th>
		<th>Telefonnummer</th>
		<th>E-Mail</th>
		<th>Geb-Tag</th>
		<th>Geb-Monat</th>
		<th>Geb-Jahr</th>
	</tr>
</thead>
<tbody>
<%
	String nachname = request.getParameter("nachname_benutzer");
    Connection c = DriverManager.getConnection(
	("jdbc:sqlite:C:\\Users\\phili\\eclipse-workspace\\DVD_Verleih_Final\\verleih.db"));

	String s = "SELECT * FROM Benutzer WHERE nachname = ?";
	PreparedStatement stmt = c.prepareStatement(s);
	stmt.setString(1, nachname);
	ResultSet rs = stmt.executeQuery();

	while(rs.next())
	{
		out.println("<tr>");
		out.println("<td>" + rs.getInt("benutzerID") + "</td>");
		out.println("<td>" + rs.getString("vorname") + "</td>");
		out.println("<td>" + rs.getString("nachname") + "</td>");
		out.println("<td>" + rs.getInt("telnummer") + "</td>");
		out.println("<td>" + rs.getString("email") + "</td>");
		out.println("<td>" + rs.getInt("geb_tag") + "</td>");
		out.println("<td>" + rs.getInt("geb_monat") + "</td>");
		out.println("<td>" + rs.getInt("geb_jahr") + "</td>");
		out.println("</tr>");
	}
		rs.close();

		c.close();
%>
</tbody>
</table>

</body>
</html>