<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="DB_Manager_package.DB_Manager"%>
    <%@page import="DB_Manager_package.DB_Manager"%>
    <%@page import="java.sql. *"%>
    <%@page import="org.sqlite. *"%>
    <%Class.forName("org.sqlite.JDBC"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DVD-Verleih</title>
</head>
<body>
<h1>Geliehen-Liste:</h1>
<p>Hier werden alle DVD's aufgelistet, die bereits ausgeliehen sind:</p>
<table>
<thead>
	<tr>
		<th>Leih-ID</th>
		<th>Von</th>
		<th>Bis</th>
		<th>DVD-ID</th>
		<th>Geliehen von</th>
	</tr>
</thead>
<tbody>
<%
        Connection c = DriverManager.getConnection(
		("jdbc:sqlite:C:\\Users\\phili\\eclipse-workspace\\DVD_Verleih_Final\\verleih.db"));

	String s = "SELECT * FROM Geliehen;";
	Statement stmt = c.createStatement();
	ResultSet rs = stmt.executeQuery(s);

	while(rs.next())
	{
		out.println("<tr>");
		out.println("<td>" + rs.getInt("leihID") + "</td>");
		out.println("<td>" + rs.getString("datum_von") + "</td>");
		out.println("<td>" + rs.getString("datum_bis") + "</td>");
		out.println("<td>" + rs.getInt("dvdID_f") + "</td>");
		out.println("<td>" + rs.getInt("benutzerID_f") + "</td>");
		out.println("</tr>");
	}
		rs.close();

		c.close();
%>
</tbody>
</table>
</body>
</html>