<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="DB_Manager_package.DB_Manager"%>
    <%@page import="java.sql. *"%>
    <%@page import="org.sqlite. *"%>
    <%Class.forName("org.sqlite.JDBC"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>DVD-Verleih</title>
</head>
<body>
<h1>Ergebnis der Suche:</h1>
<table>
<thead>
	<tr>
		<th>DVD-ID		</th>
		<th>DVD-Titel		</th>
		<th>Erscheinungsjahr		</th>
		<th>Filmlänge		</th>
		<th>Altersbeschränkung		</th>
		<th>Gebühr		</th>
		<th>Bestand		</th>
	</tr>
</thead>
<tbody>
<%
	String titel = request.getParameter("titel_dvd");
    Connection c = DriverManager.getConnection(
	("jdbc:sqlite:C:\\Users\\phili\\eclipse-workspace\\DVD_Verleih_Final\\verleih.db"));

	String s = "SELECT * FROM DVD WHERE titel = ?";
	PreparedStatement stmt = c.prepareStatement(s);
	stmt.setString(1, titel);
	ResultSet rs = stmt.executeQuery();

	while(rs.next())
	{
		out.println("<tr>");
		out.println("<td>" + rs.getString("dvdID") + "</td>");
		out.println("<td>" + rs.getString("titel") + "</td>");
		out.println("<td>" + rs.getString("erscheinungsjahr") + "</td>");
		out.println("<td>" + rs.getString("filmlaenge") + "</td>");
		out.println("<td>" + rs.getString("altersbeschraenkung") + "</td>");
		out.println("<td>" + rs.getString("leiGebuehr") + "</td>");
		out.println("<td>" + rs.getString("bestand") + "</td>");
		out.println("</tr>");
	}
		rs.close();

		c.close();
%>
</tbody>
</table>

</body>
</html>