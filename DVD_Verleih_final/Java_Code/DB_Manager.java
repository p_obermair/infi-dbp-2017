package DB_Manager_package;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DB_Manager {
	Connection c;

	public void getCon() throws SQLException {
		PreparedStatement pstmt = null;
		String stmt = "";

		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(
					("jdbc:sqlite:C:\\Users\\phili\\eclipse-workspace\\DVD_Verleih_Final\\verleih.db"));
			c.setAutoCommit(false);

		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			System.out.println("Verbindung zur Datenbank ist nicht moeglich!");
			System.exit(0);
		}

		stmt = "PRAGMA foreign_keys = 1;";
		pstmt = c.prepareStatement(stmt);
		pstmt.executeUpdate();
	}

	public void createDVDtable() throws SQLException {
		getCon();
		
		String s = "create Table if not exists DVD(" + "dvdID integer PRIMARY KEY," + " titel varchar(200) NOT NULL,"
				+ "erscheinungsjahr integer NOT NULL," + "filmlaenge integer NOT NULL,"
				+ "altersbeschraenkung integer NOT NULL," + "leiGebuehr integer NOT NULL," + " bestand integer NOT NULL);";
		
		Statement stmt = c.createStatement();
		stmt.executeUpdate(s);
		c.commit();
		c.close();
	}

	public void createBenutzertable() throws SQLException {

		getCon();
		System.out.println(c);
		String s = "create Table if not exists Benutzer(" + "benutzerID integer PRIMARY KEY,"
		+ "vorname varchar(100) NOT NULL," + "nachname varchar(100) NOT NULL," + "telnummer integer NOT NULL,"
		+ "email varchar(100) NOT NULL," + "geb_tag integer NOT NULL," + "geb_monat integer NOT NULL,"
		+ "geb_jahr integer NOT NULL);";
		
		Statement stmt = c.createStatement();
		stmt.executeUpdate(s);
		c.commit();
		c.close();
	}

	public void createGeliehentable() throws SQLException {
		
		getCon();
		
		String s = "create Table if not exists Geliehen(" + "leihID integer PRIMARY KEY,"
				+ "datum_von date NOT NULL," + "datum_bis date NOT NULL," + "dvdID_f integer NOT NULL,"
				+ "benutzerID_f integer NOT NULL," + "FOREIGN KEY (dvdID_f) REFERENCES DVD (dvdID),"
			    + "FOREIGN KEY (benutzerID_f) REFERENCES Benutzer);";
		
		Statement stmt = c.createStatement();
		stmt.executeUpdate(s);
		c.commit();
		c.close();
	}

	public void readAndInsertDVD() throws IOException, SQLException {
		
		getCon();
		PreparedStatement pstmt = null;
		String line = "";
		String csvSplit = ",";
		String[] data = null;
		BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\phili\\eclipse-workspace\\DVD_Verleih_Final\\dvd.csv"));

		while ((line = br.readLine()) != null) {
			data = line.split(csvSplit);

			pstmt = c.prepareStatement("INSERT OR REPLACE INTO DVD (dvdID,titel,erscheinungsjahr,filmlaenge,"
					+ "altersbeschraenkung,leigebuehr,bestand) Values(?,?,?,?,?,?,?)");

			pstmt.setInt(1, Integer.parseInt(data[0]));
			pstmt.setString(2, data[1]);
			pstmt.setInt(3, Integer.parseInt(data[2]));
			pstmt.setInt(4, Integer.parseInt(data[3]));
			pstmt.setInt(5, Integer.parseInt(data[4]));
			pstmt.setInt(6, Integer.parseInt(data[5]));
			pstmt.setInt(7, Integer.parseInt(data[6]));

			pstmt.executeUpdate();
			c.commit();

		}
		c.close();
	}

	public void readAndInsertBenutzer() throws IOException, SQLException {
		getCon();
		PreparedStatement pstmt = null;
		String line = "";
		String csvSplit = ",";
		String[] data = null;
		BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\phili\\eclipse-workspace\\DVD_Verleih_Final\\benutzer.csv"));

		while ((line = br.readLine()) != null) {
			data = line.split(csvSplit);

			pstmt = c.prepareStatement("INSERT OR REPLACE INTO Benutzer (benutzerID,vorname,nachname,telnummer,"
					+ "email,geb_tag,geb_monat,geb_jahr) Values(?,?,?,?,?,?,?,?)");

			pstmt.setInt(1, Integer.parseInt(data[0]));
			pstmt.setString(2,data[1]);
			pstmt.setString(3,data[2]);
			pstmt.setString(4,data[3]);
			pstmt.setString(5,data[4]);
			pstmt.setInt(6, Integer.parseInt(data[5]));
			pstmt.setInt(7, Integer.parseInt(data[6]));
			pstmt.setInt(8, Integer.parseInt(data[7]));

			pstmt.executeUpdate();
			c.commit();
		}
		c.close();
	}
	
	public void readAndInsertGeliehen() throws IOException, SQLException {
		getCon();
		PreparedStatement pstmt = null;
		String line = "";
		String csvSplit = ",";
		String[] data = null;
		BufferedReader br = new BufferedReader(new FileReader("C:\\Users\\phili\\eclipse-workspace\\DVD_Verleih_Final\\geliehen.csv"));

		while ((line = br.readLine()) != null) {
			data = line.split(csvSplit);

			pstmt = c.prepareStatement("INSERT OR REPLACE INTO Geliehen (leihID,datum_von,datum_bis,"
					+ "dvdID_f,benutzerID_f) Values(?,?,?,?,?)");

			pstmt.setInt(1, Integer.parseInt(data[0]));
			pstmt.setString(2, data[1]);
			pstmt.setString(3,data[2]);
			pstmt.setInt(4, Integer.parseInt(data[3]));
			pstmt.setInt(5, Integer.parseInt(data[4]));

			pstmt.executeUpdate();
			c.commit();
		}
		c.close();
	}
}







