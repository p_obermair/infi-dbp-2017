package DB_Manager_package;
import java.io.IOException;
import java.sql.SQLException;

public class Main {

	public static void main(String[] args) throws SQLException, IOException 
	{
		DB_Manager dm = new DB_Manager();
		dm.createDVDtable();
		dm.createBenutzertable();
		dm.createGeliehentable();
		
		dm.readAndInsertDVD();
		dm.readAndInsertBenutzer();
		dm.readAndInsertGeliehen();
		
		
	}

}
